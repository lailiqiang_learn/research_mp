const app = getApp();
const douban = app.globalData.douban;

const _get = (url, data) => {
  return new Promise((resolve, reject) => {
    wx.request({
      header: {
        "content-type": 'json'
      },
      url,
      success: function({
        statusCode,
        data
      }) {
        if (statusCode === 200) resolve(data)
        else reject(res);
      },
      fail: function(err) {
        reject(err);
      }
    })
  })
}

const _post = (url, data) => {

}

/**
 * 豆瓣-正在热映
 */
const in_theaters = () => {
  let url = douban.HOST + '/v2/movie/in_theaters';
  return _get(url);
}

/**
 * 豆瓣-电影详情
 */
const subject = (id) => {
  let url = douban.HOST + '/v2/movie/subject/' + id;
  return _get(url);
}

module.exports = {
  in_theaters,
  subject
};