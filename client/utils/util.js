const computedSize = size => {
  return size < 1024 ? size + ' B' :
    size < 1024 * 1024 ? (size / 1024).toFixed(2) + ' KB' :
    size < 1024 * 1024 * 1024 ? (size / 1024 / 1024).toFixed(2) + ' MB' :
    size < 1024 * 1024 * 1024 * 1024 ? (size / 1024 / 1024 / 1024).toFixed(2) + ' GB' : '';
}

const toString = Object.prototype.toString;

/**
 * 判断是否为日期类型
 * @param {*} parameter
 */
const isDate = parameter => {
  return toString.call(parameter) === "[object Date]";
};

const formatDate = (date = new Date(), fmt = "yyyy-MM-dd") => {
  if (!isDate(date)) date = new Date(date);
  try {
    date.getMonth();
  } catch (e) {
    return new Error("参数错误");
  }
  const o = {
    "M+": date.getMonth() + 1, //月份
    "d+": date.getDate(), //日
    "h+": date.getHours(), //小时
    "m+": date.getMinutes(), //分
    "s+": date.getSeconds(), //秒
    "q+": Math.floor((date.getMonth() + 3) / 3), //季度
    S: date.getMilliseconds() //毫秒
  };
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(
      RegExp.$1,
      (date.getFullYear() + "").substr(4 - RegExp.$1.length)
    );
  }
  for (var k in o) {
    if (new RegExp("(" + k + ")").test(fmt)) {
      fmt = fmt.replace(
        RegExp.$1,
        RegExp.$1.length === 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length)
      );
    }
  }
  return fmt;
};

// 显示成功提示
var showSuccess = (text = '成功') => wx.showToast({
  title: text,
  icon: 'success'
})

// 显示失败提示
var showFail = (content = '失败') => {
  hideBusy();
  wx.showModal({
    title: '提示',
    content,
    showCancel: false
  })
}

/**
 * 根据路径判断文件类型
 */
const getFileFormat = (path) => {
  let label = 'view';
  const ext = path.substring(path.lastIndexOf('\.') + 1).toUpperCase();
  switch (ext) {
    case 'JPG':
    case 'JPEG':
    case 'PNG':
    case 'BMP':
    case 'GIF':
    case 'TIF':
    case 'TIFF':
      label = 'image';
      break;
    case 'MP3':
    case 'MPEG':
    case 'M4A':
    case 'MID':
      label = 'audio'
      break;
    case 'MP4':
      label = 'video';
      break
    case 'DOC':
    case 'DOCX':
    case 'XLS':
    case 'XLSX':
    case 'PPT':
    case 'PPTX':
    case 'PDF':
    case 'TXT':
    case 'MD':
      label = 'doc';
      break;
  }
  return label;
}

module.exports = {
  formatDate,
  computedSize,
  showSuccess,
  showFail,
  getFileFormat
}