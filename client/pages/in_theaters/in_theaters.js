const {
  in_theaters
} = require('../../utils/service.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {},
    moveList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    wx.showLoading({
      title: '数据加载中',
    })
    in_theaters().then(({
      subjects
    }) => {
      wx.hideLoading();
      this.setData({
        moveList: subjects
      })
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const _this = this;
    wx.getUserInfo({
      success: function(res) {
        _this.setData({
          user: JSON.parse(res.rawData)
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面下拉刷新函数
   */
  onPullDownRefresh: function() {
    console.log('上拉刷新')
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    console.log('获取更多数据');
    wx.login({
      success: function(res) {
        console.log(res);
      }
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  getUserInfo: function(res) {
    console.log(res);
  }
})