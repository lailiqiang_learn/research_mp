Page({
  data: {
    todoList: [{
      text: '小程序',
      key: 'todo_1'
    }, {
      text: '数据爬虫',
      key: 'todo_2'
    }, {
      text: 'ReactNative',
      key: 'todo_3'
    }],
    value_todo: '',
    completeList: [{
      text: '吃饭',
      key: 'complete_1'
    }]
  },
  bindKeyInput: function(e) {
    // 实现双向数据绑定
    this.setData({
      value_todo: e.detail.value
    })
  },
  add: function() {
    // 添加代办任务
    if (!this.data.value_todo) return false;
    this.setData({
      todoList: [{
        text: this.data.value_todo,
        key: 'todo_' + this.data.todoList.length + 1
      }].concat(this.data.todoList),
      value_todo: ''
    })
    console.log(this.data.value_todo);
  },
  switchStatus: function(ev) {
    // 代办/已办相互转换
    console.log(ev.currentTarget.dataset);
    const dataset = ev.currentTarget.dataset,
      type = dataset.type,
      index = dataset.index;
    if (type === 'complete') {
      const newItem = this.data.todoList.splice(index, 1);
      this.setData({
        todoList: this.data.todoList,
        completeList: newItem.concat(this.data.completeList)
      })
    } else {
      const newItem = this.data.completeList.splice(index, 1);
      this.setData({
        completeList: this.data.completeList,
        todoList: newItem.concat(this.data.todoList)
      })
    }
  }
})