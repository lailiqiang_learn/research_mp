Page({
  data: {
    list: [{
        name: 'TODO List',
        path: '/pages/todo/todo'
      },
      {
        name: 'File 上传/下载/预览',
        path: '/pages/file/file'
      },
      {
        name: '支付/分销',
        path: '/pages/pay/pay'
      },
      {
        name: '豆瓣电影',
        path: '/pages/in_theaters/in_theaters'
      }
    ]
  }
})