const util = require('../../utils/util.js')

/**
 * 添加一些额外字段用于页面展示
 */
const handleFileList = (list) => {
  return list.map(item => {
    item.label = util.getFileFormat(item.filePath);
    item.formatedDate = util.formatDate(item.createTime * 1000);
    item.computedSize = util.computedSize(item.size);
    item.ext = item.filePath.substring(item.filePath.lastIndexOf('\.') + 1)
    return item;
  })
}

// 预览图片地址集合
const previewImageUrls = [];

/**
 * 获取列表中图片的地址集合
 */
const getPreviewUrls = (list) => {
  previewImageUrls.length = 0;
  list.forEach(item => {
    if (util.getFileFormat(item.filePath) === 'image') {
      previewImageUrls.push(item.filePath);
    }
  })
}

// pages/file/file.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    downloadFiles: [{
        url: 'http://ofnrozs7q.bkt.clouddn.com/%E6%98%8E%E5%8D%9A%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95%E9%97%AE%E9%A2%98%E5%8F%8D%E9%A6%88-2018-06-21.xlsx',
        ext: 'xlsx',
        "ContentType": 'application/x-xls'
      },
      {
        url: 'http://ofnrozs7q.bkt.clouddn.com/demo.mp3',
        ext: 'mp3',
        "ContentType": 'audio/mp3'
      },
      {
        url: 'http://ofnrozs7q.bkt.clouddn.com/NodeJs%E5%85%A5%E9%97%A8.pdf',
        ext: 'pdf',
        "ContentType": 'application/pdf'
      },
      {
        url: 'https://www.apowersoft.cn/wp-content/uploads/2017/09/combine-gifs-5.gif',
        ext: 'gif',
        "ContentType": 'image/gif'
      },
      {
        url: 'http://ofnrozs7q.bkt.clouddn.com/demo.txt',
        ext: 'txt',
        "ContentType": 'text/plain'
      },
      {
        url: 'http://ofnrozs7q.bkt.clouddn.com/ziyuan59musicMID.mid',
        ext: 'mid',
        "ContentType": 'audio/mid'
      },
      {
        url: 'http://ofnrozs7q.bkt.clouddn.com/ziyuan57musicM4A.m4a',
        ext: 'm4a',
        "ContentType": 'audio/mp3'
      }
    ],
    savedFileList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log('onLoad')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    console.log('onReady')
    this.getSavedFileList();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    console.log('onShow')
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    console.log('onHide')
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
    console.log('onUnload')
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  /**
   * 选择文件
   */
  select_file: function(ev) {
    const _this = this;
    const {
      type
    } = ev.currentTarget.dataset;
    // 图片文件
    type === 'image' &&
      wx.chooseImage({
        success: function(res) {
          res.tempFilePaths.forEach(tempFilePath => {
            wx.saveFile({
              tempFilePath,
              success: function(res) {
                _this.getSavedFileList();
              }
            })
          })
        },
      })
    // 视频文件
    type === 'video' && wx.chooseVideo({
      success: function({
        tempFilePath
      }) {
        wx.saveFile({
          tempFilePath,
          success: function(res) {
            _this.getSavedFileList();
          }
        })
      },
    })
  },

  /**
   * 下载文件
   */
  download_file: function(ev) {
    const _this = this;
    const {
      index
    } = ev.currentTarget.dataset;
    console.log('index', index)
    wx.showToast({
      title: '准备下载',
      mask: true,
      icon: 'none'
    })
    const {
      url,
      ContentType
    } = this.data.downloadFiles[index];
    const downloadTask = wx.downloadFile({
      url,
      header: {
        "Content-Type": ContentType
      },
      success: function({
        tempFilePath
      }) {
        wx.saveFile({
          tempFilePath,
          success: function(res) {
            wx.hideLoading();
            _this.getSavedFileList();
            util.showSuccess('下载成功')
          }
        })
      },

    });

    downloadTask.onProgressUpdate((res) => {
      wx.showToast({
        title: '已下载 ' + res.progress + '%',
        mask: true,
        icon: 'none'
      })
    })
  },

  /**
   * 获取本地已保存的文件列表
   */
  getSavedFileList: function() {
    const _this = this;
    wx.getSavedFileList({
      success: function(res) {
        const savedFileList = handleFileList(res.fileList);
        _this.setData({
          savedFileList
        })
        getPreviewUrls(savedFileList);
        console.log(savedFileList)
      }
    })
  },

  /**
   * 预览图片
   */
  previewImage: function(ev) {
    const {
      path: current
    } = ev.currentTarget.dataset;
    wx.previewImage({
      current,
      urls: previewImageUrls,
    })
  },

  /**
   * 预览文档
   */
  openDocument: function(ev) {
    const {
      path: filePath
    } = ev.currentTarget.dataset;
    wx.openDocument({
      filePath,
      success: function(res) {
        console.log(res);
      }
    })
  },

  /**
   * 删除已保存的文件
   */
  del_item: function(ev) {
    const _this = this;
    const {
      path: filePath
    } = ev.currentTarget.dataset;
    wx.removeSavedFile({
      filePath,
      success: function(res) {
        util.showSuccess('已删除')
        _this.getSavedFileList();
      }
    })
  }
})