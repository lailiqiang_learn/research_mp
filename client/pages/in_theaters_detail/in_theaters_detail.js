const {
  subject
} = require('../../utils/service.js');

// 预览图片地址集合
const previewImageUrls = [];

Page({

  /**
   * 页面的初始数据
   */
  data: {
    item: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.setNavigationBarTitle({
      title: options.title,
    });
    this.fetchSubject(options.id)
    wx.showLoading({
      title: '',
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  /**
   * 获取详情
   */
  fetchSubject: function(id) {
    subject(id).then(res => {
      wx.hideLoading();
      this.setData({
        item: res
      })
      res.photos.forEach(item => {
        previewImageUrls.push(item.image);
      })
    });
  },


  /**
   * 预览图片
   */
  previewImage: function(ev) {
    const {
      path: current
    } = ev.currentTarget.dataset;
    wx.previewImage({
      current,
      urls: previewImageUrls,
    })
  },
})