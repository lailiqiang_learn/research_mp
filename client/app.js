//app.js
var qcloud = require('./vendor/wafer2-client-sdk/index')
var config = require('./config')

App({
  onLaunch: function() {
    console.log('程序已启动', new Date().toLocaleString());
    qcloud.setLoginUrl(config.service.loginUrl)
  },
  globalData: {
    douban: {
      HOST: 'https://douban.uieee.com'
    }
  }
})